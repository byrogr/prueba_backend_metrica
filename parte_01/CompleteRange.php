<?php

/**
 * Class CompleteRange
 * @author Roger Rojas <roger.rojas.effio@gmail.com>
 */

class CompleteRange
{
    public static function build(array $numeros)
    {
        $arrSalida = [];
        $numInicial = $numeros[0];
        $numFinal = $numeros[count($numeros) - 1];

        for ($i = $numInicial; $i <= $numFinal; $i++) {
            $arrSalida[] = $i;
        }
        echo "<b>entrada:</b> [" . implode(",", $numeros) . "]" . " <b>salida:</b> [" . implode(",", $arrSalida) . "]<br>";
    }
}

CompleteRange::build([1, 2, 4, 5]);
CompleteRange::build([2, 4, 9]);
CompleteRange::build([55, 58, 60]);
