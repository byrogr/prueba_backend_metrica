<?php

/**
 * Class ChangeString
 * @author Roger Rojas <roger.rojas.effio@gmail.com>
 */
class ChangeString
{
    private static $arrABCMin = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','�','o','p','q','r','s','t','u','v','w','x','y','z'];
    private static $arrABCMay = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','�','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    private static function mayusOMin($cadena)
    {
        if (ctype_upper($cadena)) {
            return self::$arrABCMay;
        }
        return self::$arrABCMin;
    }

    private static function buscarLetra($letra)
    {
        $arrABC = self::mayusOMin($letra);
        for($i = 0; $i < count($arrABC); $i++) {
            if ($letra === $arrABC[$i]) {
                return $i;
            }
        }
        return -1;
    }

    public static function build($cadena)
    {
        $oldString = $cadena;
        for($i = 0; $i < strlen($cadena); $i++) {
            $letra = $cadena{$i};
            $arrTemp = self::mayusOMin($letra);
            $pos = self::buscarLetra($letra);
            if ($pos != -1) {
                if ( $pos == count($arrTemp) - 1 ) {
                    $cadena{$i} = $arrTemp[0];
                } else {
                    $cadena{$i} = $arrTemp[$pos + 1];
                }
            }
        }
        echo "<b>entrada:</b> \"" . $oldString . "\"" . " <b>salida:</b> \"" . $cadena . "\"<br>";
    }

}
ChangeString::build("123 abcd*3");
ChangeString::build("**Casa 52");
ChangeString::build("**Casa 52Z");