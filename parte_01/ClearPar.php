<?php

/**
 * Class ClearPar
 * @author Roger Rojas <roger.rojas.effio@gmail.com>
 */

class ClearPar
{
    public static function build($cadena)
    {
        $oldString = $cadena;
        $newString = "";
        $cantParentesis = substr_count($cadena, '()');
        for ( $i = 0; $i < $cantParentesis; $i++ ) {
            $newString .= "()";
        }
        echo "<b>entrada:</b> \"" . $oldString . "\"" . " <b>salida:</b> \"" . $newString . "\"<br>";
    }
}

ClearPar::build("()())()");
ClearPar::build("()(()");
ClearPar::build(")(");
ClearPar::build("((()");