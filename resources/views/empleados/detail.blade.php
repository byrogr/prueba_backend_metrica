@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                Detalle Empleado: {{ $empleado->name }}
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><b>Name</b></td>
                            <td>{{ $empleado->name }}</td>
                        </tr>
                        <tr>
                            <td><b>Email</b></td>
                            <td>{{ $empleado->email }}</td>
                        </tr>
                        <tr>
                            <td><b>Phone</b></td>
                            <td>{{ $empleado->phone }}</td>
                        </tr>
                        <tr>
                            <td><b>Address</b></td>
                            <td>{{ $empleado->address }}</td>
                        </tr>
                        <tr>
                            <td><b>Position</b></td>
                            <td>{{ $empleado->position }}</td>
                        </tr>
                        <tr>
                            <td><b>Salary</b></td>
                            <td>{{ $empleado->salary }}</td>
                        </tr>
                        <tr>
                            <td><b>Skills</b></td>
                            <td>
                                @foreach($empleado->skills as $skill)
                                    <span class="badge badge-pill badge-primary">{{ $skill->skill }}</span>
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a href="{{ route('listado') }}" class="card-link">&larr; Regresar</a>
            </div>
        </div>
    </div>
</div>
@endsection