@extends('layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                Filtros
            </div>
            <div class="card-body">
                <form action="{{ route('buscar') }}" method="get">
                    <div class="form-row">
                        <div class="col-lg-11">
                            <input type="text" name="email" id="txtEmail" class="form-control" placeholder="Buscar ...">
                        </div>
                        <div class="col-lg-1">
                            <input type="submit" value="Buscar" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table mt-2">
            <thead class="thead-light">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Position</th>
                <th>Salary</th>
                <th>Actions</th>
            </tr>
            </thead>
            @foreach($data as $emp)
                <tr>
                    <td>{{ $emp->name }}</td>
                    <td>{{ $emp->email }}</td>
                    <td>{{ $emp->position }}</td>
                    <td>{{ $emp->salary }}</td>
                    <td>
                        <a href="{{ route('detalle', ['id' => $emp->id]) }}"
                           class="btn btn-primary btn-block">
                            Detalle
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection