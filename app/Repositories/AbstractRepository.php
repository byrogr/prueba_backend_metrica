<?php
/**
 * @author Roger Rojas <roger.rojas.effio@gmail.com>
 */

namespace App\Repositories;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\Storage;

abstract class AbstractRepository
{
    protected $data;

    public function __construct()
    {
        $contents = Storage::disk('local')->get('data/employees.json');
        $this->data = json_decode($contents);
    }

    public function getAll()
    {
        return $this->data;
    }

    public function get($id)
    {
        foreach ($this->data as $row) {
            if ($id === $row->id) {
                return $row;
            }
        }
        return null;
    }
}