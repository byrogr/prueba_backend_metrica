<?php

/**
 * @author Roger Rojas <roger.rojas.effio@gmail.com>
 */

namespace App\Repositories;

class EmpleadoRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findByEmail($email)
    {
        foreach ($this->data as $row) {
            if ($email === strtolower($row->email)) {
                return $row;
            }
        }
        return null;
    }

    public function findBySalaryRange($min, $max)
    {
        $result = [];
        foreach ($this->data as $row) {
            $salary = str_replace('$', '', $row->salary);
            $salary = floatval(str_replace(',','', $salary));
            if ($salary >= $min && $salary <= $max) {
                $result[] = $row;
            }
        }
        return $result;
    }
}