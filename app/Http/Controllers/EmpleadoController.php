<?php

namespace App\Http\Controllers;

use App\Repositories\EmpleadoRepository;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    private $repo;

    public function __construct(EmpleadoRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $data = $this->repo->getAll();
        return view('empleados.index', compact('data'));
    }

    public function detail($id)
    {
        $empleado = $this->repo->get($id);
        return view('empleados.detail', compact('empleado'));
    }

    public function search(Request $request)
    {
        $email = strtolower($request->get('email'));
        $empleado = $this->repo->findByEmail($email);
        if (!$empleado) {
            abort(404);
        }
        return view('empleados.detail', compact('empleado'));
    }

    public function salary($min, $max)
    {
        $data = $this->repo->findBySalaryRange($min, $max);
        return response()->xml($data);
    }
}
