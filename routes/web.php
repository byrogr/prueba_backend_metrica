<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'empleados'], function() {
    Route::get('/', 'EmpleadoController@index')->name('listado');
    Route::get('/buscar', 'EmpleadoController@search')->name('buscar');
    Route::get('/{id}', 'EmpleadoController@detail')->name('detalle');
    Route::get('/api/salario/min/{min}/max/{max}', 'EmpleadoController@salary')
        ->name('api')
        ->where(['min' => '[0-9]+', 'max' => '[0-9]+']);
});
