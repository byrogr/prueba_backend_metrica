# Solucion Prueba Programador Backend PHP
Repositorio de la resolucion de la prueba para el puesto de programador backend PHP.

### Estructura de rutas de la aplicacion de empleados
Listado de empleados
```
http://localhost:port/empleados/
```
Servicio Web para buscar empleados segun rango de salario
```
http://localhost:port/empleados/api/salario/min/{min}/max/{max}
```
La resolucion de la parte 1 de la prueba se encuentra en la carpeta parte_01/ de la raiz
